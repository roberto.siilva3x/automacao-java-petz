#language en
@Rob
Feature: Petz
  Scenario: Consultar produtos
    Given Acessar home da pagina
    When  Consulta por "racao"
    And   Selecionar o terceiro produto da lista retornada
    Then  Validar nome do produto com "Ração Royal Canin Medium - Cães Adultos"
    And   Validar nome do fornecedor com "Royal Canin"
    And   Validar preco normal com "R$ 302,61"
    And   Validar preco para assinante com "R$ 272,35"
    And   Inserir o produto no carrinho de compras
    Then  Validar se os dados do item tres continuam identicos

