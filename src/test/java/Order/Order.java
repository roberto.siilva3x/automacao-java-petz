package Order;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public abstract class Order {
    public abstract void abrirUrl(String url) throws IOException;
    public abstract void escrever(WebElement elemento, String produto) throws IOException;
    public abstract void clicar(WebElement elemento) throws IOException;
    public abstract void apagarCampo(WebElement elemento) throws IOException;
    public abstract void esperarUmSegundo() throws InterruptedException;
}
