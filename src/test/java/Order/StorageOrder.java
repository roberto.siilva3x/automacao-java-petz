package Order;

import Support.DriverWeb;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import java.io.IOException;

public class StorageOrder extends Order{
    @Override
    public void abrirUrl(String url) throws IOException {
        DriverWeb.getDriver().get(url);

    }

    @Override
    public void escrever(WebElement elemento, String produto) throws IOException {
        elemento.sendKeys(produto);

    }

    @Override
    public void clicar(WebElement elemento) throws IOException {
        elemento.click();
    }


    @Override
    public void apagarCampo(WebElement elemento) throws IOException {
        elemento.clear();

    }

    @Override
    public void esperarUmSegundo() throws InterruptedException {
        Thread.sleep(1000);
    }
}
