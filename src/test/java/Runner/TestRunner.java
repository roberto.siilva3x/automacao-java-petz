package Runner;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.runner.RunWith;
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "src/test/resources/features",
        snippets = CucumberOptions.SnippetType.CAMELCASE,
        tags = {"@Rob"},
        glue = {"Steps"},
        plugin = "pretty"
        ,
        //strict = true,dryRun = true
        monochrome = true)
public class TestRunner {
}
