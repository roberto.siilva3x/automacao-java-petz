package Runner;

import Pages.BuscaPage;
import Pages.MainPage;
import Pages.MeuCarrinhoPage;
import Support.DriverWeb;
import Support.IinteracaoSeleniumJavaWeb;
import Support.URL_LINK;
import Tdm.TDM;
import Util.UtilsScreenShot;
import org.easetech.easytest.annotation.DataLoader;
import org.easetech.easytest.annotation.Param;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.IOException;

@RunWith(DataDrivenTestRunner.class)
@DataLoader( filePaths = {"InformacoesPetzTestData.csv"})
public class TestCsv {
    private WebDriver navegador;
    TDM tdm = new TDM();
    IinteracaoSeleniumJavaWeb i = new IinteracaoSeleniumJavaWeb();

    @Before
    public void setUp() throws IOException {
     navegador =  DriverWeb.getDriver();

    }
    @Test
    public void testRealizarCompraDeProdutos(@Param(name="produto")String produto,
                                             @Param(name="descricao") String descricao,
                                             @Param(name="fornecedor")String fornecedor,
                                             @Param(name="valor")String valor,
                                             @Param(name="valorAssinante")String valorAssinante) throws IOException {
        i.abrirUrl(URL_LINK.PETZ_MAIN_URL);
        i.esperarElemento(MainPage.INPUT_SEARCH,20);
        i.escrever(MainPage.INPUT_SEARCH,produto);
        i.clicar(MainPage.BUTTON_SEARCH);
        UtilsScreenShot.takeScreenShot();
        i.esperarElemento(BuscaPage.ITEM_X_LISTA,20);
        i.clicar(BuscaPage.ITEM_X_LISTA);
        UtilsScreenShot.takeScreenShot();
        i.esperarElemento(MeuCarrinhoPage.NOME_PRODUTO,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.NOME_PRODUTO,descricao,"innerText");
        TDM.produto.setNome(i.getAttribute(MeuCarrinhoPage.NOME_PRODUTO,"innerText"));
        UtilsScreenShot.takeScreenShot();

        i.esperarElemento(MeuCarrinhoPage.NOME_FORNECEDOR,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.NOME_FORNECEDOR,fornecedor,"innerText");
        TDM.fornecedor.setNome(i.getAttribute(MeuCarrinhoPage.NOME_FORNECEDOR,"innerText"));
        UtilsScreenShot.takeScreenShot();

        i.esperarElemento(MeuCarrinhoPage.PRECO_PRODUTO_NORMAL,20);
        String nome = i.getAttribute(MeuCarrinhoPage.PRECO_PRODUTO_NORMAL,"innerText").replace(",", ".");
        Assert.assertEquals(nome,valor);
        TDM.produto.setValorNormal(i.getAttribute(MeuCarrinhoPage.PRECO_PRODUTO_NORMAL,"innerText"));
        UtilsScreenShot.takeScreenShot();

        i.esperarElemento(MeuCarrinhoPage.PRECO_PRODUTO_ASSINANTE,20);
        nome = i.getAttribute(MeuCarrinhoPage.PRECO_PRODUTO_ASSINANTE,"innerText").replace(",", ".");
        Assert.assertEquals(nome,valorAssinante);
        TDM.produto.setValorAssinante(i.getAttribute(MeuCarrinhoPage.PRECO_PRODUTO_ASSINANTE,"innerText"));
        UtilsScreenShot.takeScreenShot();

        i.esperarElemento(MeuCarrinhoPage.BTN_MEU_CARRINHO,20);
        i.clicar(MeuCarrinhoPage.BTN_MEU_CARRINHO);
        UtilsScreenShot.takeScreenShot();

    }

    @After
    public void tearDown(){
        DriverWeb.endSession();
    }
}
