package Util;

import Interace.Acoes;
import Support.DriverWeb;
import com.sun.javafx.util.Utils;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.Scenario;
import io.cucumber.plugin.event.PickleStepTestStep;
import io.cucumber.plugin.event.TestCase;
import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;

import java.io.File;
import java.lang.reflect.Field;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class UtilsScreenShot extends Utils {
    static Log Logger = LogFactory.getLog(Acoes.class);
    //static Log Logger = LogFactory.getLog(UtilsScreenShot.class);
    private static String caminho;
    private static SimpleDateFormat timestampEvidencia;
    public static SimpleDateFormat timeStampPasta;
    private static Scenario scenario;
    private static PickleStepTestStep currentStep;
    private int counter = 0;
    @Before
    public void before(final Scenario scenario){
        UtilsScreenShot.scenario = scenario;
        System.out.println("scenario ="+scenario);
    }

    public static void takeScreenShot(){
        String stepName = "";
       // String nomeFeature = getFeatureFileNameFromScenarioId();
        //String nomePasta = returnNomeCenario();
        String nomePasta = "fotos";
        try{
            stepName = currentStep.getStepText();
        }
        catch (NullPointerException ignored){
            Logger.warn("nome do step nao encontrado");
        }
        String nomePrint = "Evidencia";
        Date date = new Date();
        File scrFile = (File)((TakesScreenshot) DriverWeb.getDriver()).getScreenshotAs(OutputType.FILE);
        try{
           // FileUtils.copyFile(scrFile, new File(caminho + "/" +nomeFeature + "/" + nomePasta + "/"
            FileUtils.copyFile(scrFile, new File(caminho  + "/" + nomePasta + "/"
            + "_" + timeStampPasta.format(date) + "/" + nomePrint + "_" + stepName + " "
                    + timestampEvidencia.format(date)+ ".png"));
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }
    static {
        caminho = "src" + File.separator + "test" + File.separator + "resources" + File.separator + "relatorios"
                + File.separator + "screenshot" + File.separator;
        timestampEvidencia = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss-SSS");
        timeStampPasta = new SimpleDateFormat("yyMMdd");
    }
    public static String returnNomeCenario(){
        String nome = scenario.getName();
        return nome;
    }
    public static String getFeatureFileNameFromScenarioId(){
        String featureName;
        String rawFeatureName = scenario.getId().replaceAll("/Users/robertofilho/Documents/repositorios/automacao-java-petz/src/test/resources","").trim();
        featureName = rawFeatureName.substring(0,rawFeatureName.indexOf(":"));
        featureName = featureName.replaceAll(".feature","").trim();
        return featureName;
    }
    @BeforeStep
    public void getStepName(Scenario scenario) throws NoSuchFieldException, IllegalAccessException {
        Field f = scenario.getClass().getDeclaredField("testCase");
        f.setAccessible(true);
        TestCase r = (TestCase) f.get(scenario);
        List<PickleStepTestStep> stepDefs = r.getTestSteps()
        .stream()
                .filter( x -> x instanceof  PickleStepTestStep)
                .map(x-> (PickleStepTestStep) x)
                .collect(Collectors.toList());
        currentStep = stepDefs.get(counter);
    }
    @AfterStep
    public void afterStep(Scenario scenario){
        counter += 1;
    }

}
