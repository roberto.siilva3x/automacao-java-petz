package Interace;

import Support.DriverWeb;
import Util.UtilsScreenShot;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.junit.Assert;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.IOException;
import java.security.Key;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import static org.junit.Assert.fail;

public interface Acoes {
    Log Logger = LogFactory.getLog(Acoes.class);
    default void esperarElemento(WebElement elemento, int timeout) throws IOException {
        try {
            WebDriverWait wait = new WebDriverWait(DriverWeb.getDriver(), timeout);
            wait.until(ExpectedConditions.visibilityOf(elemento));
        }
        catch (NoSuchElementException e){
            Logger.info("-- o elemento" + e + "nao existe na tela");
            fail("nao encontrou elemento");
        }
        Logger.info("-- o elemento" + elemento + "nao existe na tela");
    }

    default void moverParaElemento(WebElement elemento) throws IOException {
        org.openqa.selenium.interactions.Actions action = new org.openqa.selenium.interactions.Actions(DriverWeb.getDriver());
        action.moveToElement(elemento).build().perform();
    }

    default void validarTextoAtravesAtributo(WebElement elemento, String texto, String tipoatributo) throws IOException {
        String name = elemento.getAttribute(tipoatributo);
        Assert.assertEquals(texto, name);
    }

    default String getAttribute(WebElement elemento,String tipoatributo){
        String nome = elemento.getAttribute(tipoatributo);
        return nome;
    }

    default void selecionarNovaAbaEfecharAntiga() throws IOException {
        ArrayList<String> tabs2 = new ArrayList<String>(DriverWeb.getDriver().getWindowHandles());
        DriverWeb.getDriver().switchTo().window(tabs2.get(0));
        DriverWeb.getDriver().close();
        DriverWeb.getDriver().switchTo().window(tabs2.get(1));
    }

    default void apagarComBackSpace(WebElement elemento,int times){

        for (int x=0; x<= times; x++){
            elemento.sendKeys(Keys.BACK_SPACE);
        }

    }
    default void selecionarItemDaLista(WebElement elemento,String texto){

    }
    default boolean validarSeElementoExisteEmTela(WebElement elemento, int timeout){
        Logger.info("-- Validando se o elemento" + elemento + "existe na tela");
        WebDriverWait wait = new WebDriverWait(DriverWeb.getDriver(),timeout);
        try{
            wait.until(ExpectedConditions.visibilityOf(elemento));
            UtilsScreenShot.takeScreenShot();
        }
        catch (NoSuchElementException e){
            Logger.info("-- o elemento" + elemento + "nao existe na tela");
            return false;
        }
        Logger.info(" -- O elemento "+ elemento + "existe na tela");
        return true;
    }
}
