package Pages;

import Support.DriverWeb;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class ComprarAgoraPage {
    static WebDriver driver = DriverWeb.getDriver();
    public static WebElement NOME_PRODUTO_COMPRAR_AGORA = driver.findElement(By.xpath("//table[@class=\"table table-sacola\"]//td[2]//a"));
    public static WebElement NOME_FORNECEDOR_COMPRAR_AGORA = driver.findElement(By.xpath("//div[2][@class=\"col-md-6 reset-padding\"]//span"));
    public static WebElement PRECO_PRODUTO_NORMAL_COMPRAR_AGORA = driver.findElement(By.xpath("//table[@class=\"table table-sacola\"]//td[3]"));
    public static WebElement PRECO_PRODUTO_ASSINANTE_COMPRAR_AGORA = driver.findElement(By.xpath("//div[@class=\"opts\"]//span"));
}
