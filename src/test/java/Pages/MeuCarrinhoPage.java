package Pages;

import Support.DriverWeb;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class MeuCarrinhoPage {
    static WebDriver driver = DriverWeb.getDriver();
    public static WebElement NOME_PRODUTO = driver.findElement(By.xpath("//h1"));
    public static WebElement NOME_FORNECEDOR = driver.findElement(By.xpath("//div[@class = \"product-code-brand\"]/a[2]/span"));
    public static WebElement PRECO_PRODUTO_NORMAL = driver.findElement(By.xpath("//div[@class = \"price\"]//strong"));
    public static WebElement PRECO_PRODUTO_ASSINANTE = driver.findElement(By.xpath("//span[@class = \"price-subscriber\"]"));
    public static WebElement BTN_MEU_CARRINHO = driver.findElement(By.id("adicionarAoCarrinho"));


}
