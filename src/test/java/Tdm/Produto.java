package Tdm;

public class Produto {
    private String valorNormal;
    private String valorAssinante;
    private String nome;


    public String getValorNormal() {
        return valorNormal;
    }

    public void setValorNormal(String valorNormal) {
        this.valorNormal = valorNormal;
    }

    public String getValorAssinante() {
        return valorAssinante;
    }

    public void setValorAssinante(String valorAssinante) {
        this.valorAssinante = valorAssinante;
    }


    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }


}
