package Steps;

import Pages.BuscaPage;
import Pages.MainPage;
import Pages.MeuCarrinhoPage;
import Support.IinteracaoSeleniumJavaWeb;
import Support.URL_LINK;
import Tdm.TDM;
import Util.UtilsScreenShot;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.jcp.xml.dsig.internal.dom.Utils;

import java.io.IOException;

public class PetzSteps {
    IinteracaoSeleniumJavaWeb i = new IinteracaoSeleniumJavaWeb();
    @Given("Acessar home da pagina")
    public void acessarHomeDaPagina() throws IOException {
        System.out.println("link ="+URL_LINK.PETZ_MAIN_URL);
        i.abrirUrl(URL_LINK.PETZ_MAIN_URL);
        UtilsScreenShot.takeScreenShot();
    }

    @When("Consulta por {string}")
    public void consultePor(String texto) throws IOException, InterruptedException {
        i.esperarElemento(MainPage.INPUT_SEARCH,20);
        i.escrever(MainPage.INPUT_SEARCH,texto);
        i.clicar(MainPage.BUTTON_SEARCH);
        UtilsScreenShot.takeScreenShot();
        //Util.LogUtils.incluirLogUrl("Nao encontrado botao search",MainPage.INPUT_SEARCH);
    }

    @When("Selecionar o terceiro produto da lista retornada")
    public void selecionarOItemDaListaRetornada() throws IOException {
        i.esperarElemento(BuscaPage.ITEM_X_LISTA,20);
        i.clicar(BuscaPage.ITEM_X_LISTA);
        UtilsScreenShot.takeScreenShot();
    }

    @Then("Validar nome do produto com {string}")
    public void validarNomeDoProdutoCom(String texto) throws IOException {
        i.esperarElemento(MeuCarrinhoPage.NOME_PRODUTO,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.NOME_PRODUTO,texto,"innerText");
        TDM.produto.setNome(i.getAttribute(MeuCarrinhoPage.NOME_PRODUTO,"innerText"));
        UtilsScreenShot.takeScreenShot();
    }

    @Then("Validar nome do fornecedor com {string}")
    public void validarNomeDoFornecedorCom(String texto) throws IOException {
        i.esperarElemento(MeuCarrinhoPage.NOME_FORNECEDOR,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.NOME_FORNECEDOR,texto,"innerText");
        TDM.fornecedor.setNome(i.getAttribute(MeuCarrinhoPage.NOME_FORNECEDOR,"innerText"));
        UtilsScreenShot.takeScreenShot();
    }
    @Then("Validar preco normal com {string}")
    public void validarPrecoNormalCom(String texto) throws IOException {
        i.esperarElemento(MeuCarrinhoPage.PRECO_PRODUTO_NORMAL,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.PRECO_PRODUTO_NORMAL,texto,"innerText");
        TDM.produto.setValorNormal(i.getAttribute(MeuCarrinhoPage.NOME_FORNECEDOR,"innerText"));
        UtilsScreenShot.takeScreenShot();
    }
    @Then("Validar preco para assinante com {string}")
    public void validarPrecoParaAssinanteCom(String texto) throws IOException {
        i.esperarElemento(MeuCarrinhoPage.PRECO_PRODUTO_ASSINANTE,20);
        i.validarTextoAtravesAtributo(MeuCarrinhoPage.PRECO_PRODUTO_ASSINANTE,texto,"innerText");
        TDM.produto.setValorAssinante(i.getAttribute(MeuCarrinhoPage.NOME_FORNECEDOR,"innerText"));
        UtilsScreenShot.takeScreenShot();
    }

    @Then("Inserir o produto no carrinho de compras")
    public void inserirOProdutoNoCarrinhoDeCompras() throws IOException {
        i.esperarElemento(MeuCarrinhoPage.BTN_MEU_CARRINHO,20);
        i.clicar(MeuCarrinhoPage.BTN_MEU_CARRINHO);
        UtilsScreenShot.takeScreenShot();
    }

    @Then("Validar se os dados do item tres continuam identicos")
    public void validarSeOsDadosDoItemContinuamIdenticos() {

    }

}
