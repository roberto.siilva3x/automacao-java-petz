package Steps;

import Support.DriverDirectory;
import Support.DriverWeb;
import Util.UtilsScreenShot;
import com.sun.javafx.util.Utils;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.apache.commons.io.FileUtils;
import org.easetech.easytest.runner.DataDrivenTestRunner;
import org.junit.runner.RunWith;

import java.io.File;
import java.io.IOException;
import java.sql.Driver;

@RunWith(DataDrivenTestRunner.class)
public class ServiceHooks {
    public static Scenario scenario;
    File pathTarget = new File("/Users/robertofilho/Documents/repositorios/automacao-java-petz/Chromelog.txt");
    File pathSources = new File("/Users/robertofilho/Documents/repositorios/automacao-java-petz/src/test/resources/log/logPetz.txt");
    @Before
    public void setUp(Scenario scenario){
        DriverWeb.getDriver();
        try{
            pathSources.delete();
        }
        catch (Exception e){
           System.out.println("arquivo nao existe");
        }
       // ServiceHooks.scenario = scenario;

    }


    @After
    public void tearDown() throws IOException {

        FileUtils.copyFile(pathTarget, pathSources);
        pathTarget.delete();
    }
}
